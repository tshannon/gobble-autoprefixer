var ap = require("autoprefixer");
var path = require("path");
var sander = require("sander");
var _ = require('underscore');

var cssRegExp = /\.css$/;

module.exports = autoprefixer;

// Autoprefixer works with filenames for its sourcemaps, which means it needs to
// be a directory transformer.
function autoprefixer(inputdir, outputdir, options) {

	return sander.lsr( inputdir ).then( function ( allFiles ) {
		var ops = [];

		for (var i in allFiles) {
			var filename = allFiles[i];
			if (!filename.match(cssRegExp)) {
				ops.push( sander.symlink(inputdir, filename).to(outputdir, filename) );
			} else {
				ops.push( sander.readFile(inputdir, filename).then(function(filename){
					return function(input) {

						var fileOptions = _.extend({
							from: path.join(inputdir, filename),
							to: path.join(outputdir, filename),
							map: { inline: false }
						}, options);

						var processed = ap.process(input, fileOptions);
						return sander.Promise.all([
							sander.writeFile(outputdir, filename, processed.css),
							sander.writeFile(outputdir, filename + '.map', processed.map.toString())
						]);
					}
				}(filename)));
			}
		}

		return sander.Promise.all(ops);
	});
}
